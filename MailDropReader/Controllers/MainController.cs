﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MailDropReader.Models;
using OpenPop.Mime;
using OpenPop.Mime.Header;

namespace MailDropReader.Controllers
{
    public class MainController : Controller
    {
        private string MailBox { get { return ConfigurationManager.AppSettings["mailboxdir"]; } }

        private string ContactsToString(IList<RfcMailAddress> contacts)
        {
            return contacts != null ? string.Join("; ", contacts.Select(c => c.ToString())) : "";
        }

        private MailModel LoadMail(string emailFile)
        {
            using (var str = System.IO.File.OpenRead(Path.Combine(MailBox, emailFile)))
            {
                var mail = Message.Load(str);
                return new MailModel
                {
                    FileName = emailFile.Substring(0, emailFile.LastIndexOf(".")),
                    From = mail.Headers.From.ToString(),
                    To = ContactsToString(mail.Headers.To),
                    BCC = ContactsToString(mail.Headers.Bcc),
                    Subject = mail.Headers.Subject,
                    Body = mail.MessagePart.GetBodyAsText(),
                    Date = mail.Headers.DateSent.ToLocalTime()
                };
            }
        }

        public ActionResult Index()
        {
            var model = new List<MailModel>();
            foreach (var emailFile in Directory.GetFiles(MailBox, "*.eml"))
            {
                model.Add(LoadMail(Path.GetFileName(emailFile)));
            }

            return View(model);
        }

        public ActionResult Show([Bind(Prefix="id")]string fileName)
        {
            fileName = fileName.Replace("\\", "").Replace("/", "").Replace(":", "") + ".eml";
            var model = LoadMail(fileName);
            return View(model);
        }
    }
}
