﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MailDropReader.Models
{
    public class MailModel
    {
        public string FileName { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string BCC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime Date { get; set;  }
    }
}